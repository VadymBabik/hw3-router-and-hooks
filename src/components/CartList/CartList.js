import React, {useState} from "react";
import Empty from "../Emty/Emty";
import style from "./CartList.module.scss";
import {Button} from "../Button/Button";
import CardCart from "../CardCart/CardCart";
import Modal from "../Modal/Modal";

const CartList = ({cartList, addCart, removeOneCart, removeFullCart}) => {
        const [modal, setModal] = useState(false);
        const [cod, setCod] = useState(0);

        const modalActive = (id) => {
            setCod(id)
            setModal(!modal)
        };

        const total = () => {
        let count = 0;
        cartList.forEach(e => {
            count += e.price * e.cart.count
        })
        return count
    }

    return (
        <div className={`${style.wrapperCart} container`}>
            <ul className="collection with-header">

            <li className="collection-header"><h4>Your purchases</h4></li>
            {cartList.map(product => {
                return (
                    <CardCart
                        key={product.id}
                        product={product}
                        removeOneCart={removeOneCart}
                        addToCart={addCart}
                        modal={modalActive}
                    />
                )
            })}
            <li className={`${style.footer} collection-header`}>
                <Button text={'checkout'} size={style.checkout} color={'orange accent-3'}/>
                <Empty/>
                <h4 className={`${style.totalValue}`}>{`TOTAL:${`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(total())}`}`}</h4>
            </li>
        </ul>
            {modal && (
                <Modal
                    isOpen={modalActive}
                    header={"Removing an item from the cart?"}
                    textmodal={"Are you sure you want to remove an item from your cart?"}
                    actions={
                        <div className={style.modalFooter}>
                            <Button
                                size={"l"}
                                click={() => {
                                    removeFullCart(cod);
                                    modalActive();
                                }}
                                color={"orange accent-3"}
                                text={"OK"}
                            />
                            <Button
                                size={"l"}
                                click={modalActive}
                                color={"orange accent-3"}
                                text={"Cancel"}
                            />
                        </div>
                    }
                />
            )}
        </div>
    );
}
;
export default CartList;
